FROM node:16-alpine3.11 as builder

ENV NODE_ENV build

USER root
WORKDIR /app

COPY . /app

RUN yarn
RUN yarn build


WORKDIR /app/build
RUN yarn --prod

FROM vrigaccinx/node16-python3-pandas:latest as runner

COPY --from=builder /app/build/package*.json /app/
COPY --from=builder /app/build/node_modules/ /app/node_modules/
COPY --from=builder /app/build/ /app/


CMD ["node", "server.js"]

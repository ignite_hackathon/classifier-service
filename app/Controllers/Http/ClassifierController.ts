import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import natural from 'natural'
import sw from 'stopword'
import { PythonShell } from 'python-shell'
var path = require('path')
var classifierScriptPath = path.resolve('scripts/main.py')

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace)
}

const runClassifierScript = async (movements: string) => {
  const words = {
    words: ['REDIVA', 'DEVUELTO', 'IVA'],
  }

  console.log('Executing', classifierScriptPath)

  let shell = new PythonShell(`${classifierScriptPath}`, {
    mode: 'text',
    pythonOptions: ['-u'],
    // eslint-disable-next-line prettier/prettier
    args: ['-m', movements, '-w', JSON.stringify(words)],
  })

  const result = await new Promise((resolve, reject) => {
    let buffer = ''
    shell.on('message', function (message) {
      buffer = buffer.concat(message)
    })
    shell.on('error', function (error) {
      console.log('error ', error)
      reject(error)
    })
    shell.on('close', () => {
      resolve(JSON.parse(replaceAll(buffer, `'`, `"`)))
    })
  })

  return result
}

class ClassifierController {
  //   login function
  public async classify({ request, response, auth }: HttpContextContract) {
    try {
      const data = request.body()
      const movements = JSON.stringify(data.movements)

      const result = await runClassifierScript(movements)
      response.send(result)
    } catch (error) {
      return error
    }
  }
}

export default ClassifierController

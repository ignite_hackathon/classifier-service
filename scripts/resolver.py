import json
from classifier import Classifier

class Resolver():

    def __init__(self, movements_path, words_path):
        self.movements_path = movements_path
        self.words_path = words_path


    def resolve(self):
        movements = json.loads(self.movements_path)

        words = json.loads(self.words_path)

        classi = Classifier()
        return classi.classifie(words['words'], movements)




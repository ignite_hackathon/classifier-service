# Libraries
import pandas as pd
import json
import matplotlib as plt
# Natural Language Toolkit
import nltk
nltk.download('punkt')
nltk.download('stopwords')
from collections import Counter
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import pdb

class Classifier():

    def classifie(self, words, movements):
        dataset = self.get_dataframe(movements)

        most_common_words = {}
        c_and_d = {}
        matches = []
        moves = []

        try:

            for index, text in dataset.detail.items():
                most_common_words = self.add_to_most_common_words(
                        most_common_words, text, index
                )
            matches = self.search_words_in_most_common_words(
                    most_common_words, words
            )
            moves = dataset.filter(items=matches, axis=0)

        except Exception as e:
            print(e)

        return moves.to_dict(orient='records')

    def get_dataframe(self, data):
        dataset = pd.DataFrame(data).drop(['extra_data', 'reference'], axis=1)
        return dataset

    def filtrar_palabras(self, dataset, palabras):
        re = '|'.join(palabras)
        filtro = dataset.detail.str.contains(re)
        return dataset[filtro], filtro


    # most_common_words = { 'word': (cant, 'ix,ix,...') }
    def add_to_most_common_words(self, most_common_words, text, index):
        # tokenize
        raw = ' '.join(word_tokenize(text.lower()))
        tokenizer = RegexpTokenizer(r'[A-Za-z]{2,}')
        words = tokenizer.tokenize(raw)

        # remove stopwords
        stop_words_spanish = set(stopwords.words('spanish'))
        stop_words_english = set(stopwords.words('english'))
        words = [word for word in words if word not in stop_words_spanish]
        words = [word for word in words if word not in stop_words_english]

        # count word frequency
        counter = Counter()
        counter.update(words)
        most_common = counter.most_common(10)

        for word_tuple in most_common:
            #print(f'Tuple: {word_tuple}')
            dict_index = word_tuple[0]
            word_count = word_tuple[1]
            if dict_index in most_common_words.keys():
                dict_count = most_common_words[dict_index][0]
                indexes = most_common_words[dict_index][1]

                most_common_words.update({ dict_index: ( dict_count + word_count, f'{indexes},{index}' )})
            else:
                most_common_words[dict_index] = (word_count, index)
        return most_common_words


    def search_words_in_most_common_words(self, most_common_words, words):
        matching_transactions_indexes = []
        for common_word, word_stats in most_common_words.items():
            if common_word.upper() in words:
                # get all transactions with matching word
                transactions_indexes = word_stats[1]
                to_add = word_stats[1]
                if type(word_stats[1]) is str:
                    transactions_indexes = word_stats[1].split(',')
                    ente = [int(a) for a in transactions_indexes]
                    matching_transactions_indexes.extend(ente)
                else:
                    matching_transactions_indexes.append(to_add)

        s = set(matching_transactions_indexes)
        return s

import argparse
from resolver import Resolver
import sys

def main():
    """Main --> Fuzzer"""
    # Arguments #
    parser = argparse.ArgumentParser(description='Fuzzer parameters')

    required = parser.add_argument_group(title='Basic arguments')

    required.add_argument('-m', '--movements', help='Movements json', required=True)
    required.add_argument('-w', '--words', help='Wors json file', required=True)

    args = parser.parse_args()
    res = Resolver(args.movements, args.words)
    print(res.resolve())
    sys.exit(0)


if __name__ == '__main__':
    main()


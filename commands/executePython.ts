const {spawn} = require('child_process');

const getPythonScriptStdout = (pythonScriptPath) => {
    console.log('ejecuta');
    const python = spawn('python', [pythonScriptPath]);

    return new Promise((resolve, reject) => {
        let result = ""
        python.stdout.on('data', (data) => {
          console.log(data)
            result += data
        });
        python.on('close', () => {
            resolve(result)
        });
        python.on('error', (err) => {
          console.error(err)
            reject(err)
        });
    })
}

export default getPythonScriptStdout




